# nodejs-authentication

This project is for research on Node.js authentication. Ways to secure apps by making authenticated requests. 

### Dependencies Used.
- Node.js
- Mongo DB
- Express
- Joi (validation function)
- mongoose (creating models and schema)
- JWT (generate and verify JsonWebTokens)
- bcrypt (hashing the password stored in the database)